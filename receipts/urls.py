from django.urls import path
from receipts.views import (
    receipt_view,
    create,
    category_view,
    account_view,
    create_category,
    create_account,
)


urlpatterns = [
    path("", receipt_view, name="home"),
    path("create/", create, name="create_receipt"),
    path("categories/", category_view, name="category_list"),
    path("accounts/", account_view, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
